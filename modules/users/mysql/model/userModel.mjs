import { DataTypes } from "sequelize";

import db from "../../../../utils/sequlize/db.mjs";

const User = db.sequelize.define("User", {
	email: {
		type: DataTypes.STRING,
		allowNull: false,
		unique: true,
	},
	password: {
		type: DataTypes.TEXT,
		allowNull: false,
	},
});

export default User;
