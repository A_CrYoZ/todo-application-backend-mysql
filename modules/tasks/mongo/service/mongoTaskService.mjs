import TasksModel from "../model/tasksModel.mjs";

class MongoTaskService {
	static async addTask(task) {
		try {
			await TasksModel.create({ ...task });
			return true;
		} catch (error) {
			// 11000 - Duplicate Key (when a unique constraint index is violated)
			if (error.code === 11000) return false;

			throw error;
		}
	}

	static async completeTaskByTitle(title, creator) {
		const result = await TasksModel.updateOne(
			{ title, creator },
			{ done: true }
		);

		if (result.acknowledged) {
			return true;
		}

		return false;
	}

	static async getAllTasks(creator) {
		return TasksModel.find({ creator });
	}

	static async getCompletedTasks(creator) {
		return TasksModel.find({ creator, done: true });
	}

	static async getIncompleteTasks(creator) {
		return TasksModel.find({ creator, done: false });
	}

	static async updateTask(oldName, newTitle, newDescription, creator) {
		try {
			const result = await TasksModel.updateOne(
				{ title: oldName, creator },
				{ title: newTitle, description: newDescription }
			);

			if (result.acknowledged) return true;

			return false;
		} catch (error) {
			// 11000 - Duplicate Key (when a unique constraint index is violated)
			if (error.code === 11000) throw new Error("Duplicate");

			throw error;
		}
	}

	static async deleteTaskByTitle(title, creator) {
		const result = await TasksModel.deleteOne({ title, creator });

		if (result.deletedCount > 0) return true;
		return false;
	}
}

export default MongoTaskService;
