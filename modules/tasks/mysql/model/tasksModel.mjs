import { DataTypes } from "sequelize";

import db from "../../../../utils/sequlize/db.mjs";
import User from "../../../users/mysql/model/userModel.mjs";

const Task = db.sequelize.define(
	"Task",
	{
		title: DataTypes.STRING,
		description: DataTypes.STRING,
		done: DataTypes.BOOLEAN,
		creator: DataTypes.INTEGER,
	},
	{
		indexes: [
			{
				unique: true,
				fields: ["title", "creator"],
			},
		],
	}
);

Task.belongsTo(User, {
	onDelete: "RESTRICT",
	onUpdate: "RESTRICT",
	foreignKey: "creator",
});

export default Task;
