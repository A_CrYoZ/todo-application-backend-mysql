import TasksModel from "../model/tasksModel.mjs";

class MySQLTaskService {
	static async addTask(task) {
		try {
			await TasksModel.create({ ...task });
			return true;
		} catch (error) {
			// ER_DUP_ENTRY - Duplicate Key (when a unique constraint index is violated)
			if (error.parent?.code === "ER_DUP_ENTRY") return false;

			throw error;
		}
	}

	static async completeTaskByTitle(title, creator) {
		const result = await TasksModel.update(
			{ done: true },
			{
				where: {
					title,
					creator,
				},
			}
		);

		return result[0] === 1;
	}

	static async getAllTasks(creator) {
		return TasksModel.findAll({ where: { creator } });
	}

	static async getCompletedTasks(creator) {
		return TasksModel.findAll({ where: { creator, done: true } });
	}

	static async getIncompleteTasks(creator) {
		return TasksModel.findAll({ where: { creator, done: false } });
	}

	static async updateTask(oldName, newTitle, newDescription, creator) {
		try {
			const result = await TasksModel.update(
				{ title: newTitle, description: newDescription },
				{
					where: { title: oldName, creator },
				}
			);

			return result[0] === 1;
		} catch (error) {
			// TODO: implement it
			// 11000 - Duplicate Key (when a unique constraint index is violated)
			if (error.parent?.code === "ER_DUP_ENTRY") throw new Error("Duplicate");

			throw error;
		}
	}

	static async deleteTaskByTitle(title, creator) {
		const result = await TasksModel.destroy({ where: { title, creator } });

		return result === 1;
	}
}

export default MySQLTaskService;
