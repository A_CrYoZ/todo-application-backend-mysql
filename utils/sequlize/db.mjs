import { Sequelize } from "sequelize";

import config from "../../configs/config.mjs";

const db = {};

const sequelize = new Sequelize(config);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
